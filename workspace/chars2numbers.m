function line = chars2numbers(line)
    
    LETTERS = 'A' : 'Z';
    letters = 'a' : 'z';
    
    for i = 1 : length(LETTERS)
        NUMBER = num2str(LETTERS(i) - 'A' + 1);
        number = num2str(letters(i) - 'a' + 1);
        if(length(NUMBER) == 1)
            NUMBER = strcat('0', NUMBER);
        end
        line = regexprep(line, LETTERS(i), NUMBER);
        line = regexprep(line, letters(i), number);
    end
end