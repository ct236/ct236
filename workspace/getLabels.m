function labelsMap = getLabels(year)
        global map index
        map = containers.Map('KeyType','char','ValueType','double');
        index = 0;
        
        if(ischar(year))
            year = str2double(year);
        end
        
        switch year
            case 2012
                addToMap('ID');
                addToMap('ANO_CENSO');
                addToMap('PK_COD_MATRICULA');
                addToMap('FK_COD_ALUNO');
                addToMap('NU_DIA');
                addToMap('NU_MES');
                addToMap('NU_ANO');
                addToMap('NUM_IDADE');
                addToMap('NU_DUR_ESCOLARIZACAO');
                addToMap('NU_DUR_ATIV_COMP_MESMA_REDE');
                addToMap('NU_DUR_ATIV_COMP_OUTRAS_REDES');
                addToMap('TP_SEXO');
                addToMap('TP_COR_RACA');
                addToMap('TP_NACIONALIDADE');
                addToMap('FK_COD_PAIS_ORIGEM');
                addToMap('FK_COD_ESTADO_NASC');
                addToMap('SGL_UF_NASCIMENTO');
                addToMap('FK_COD_MUNICIPIO_DNASC');
                addToMap('FK_COD_ESTADO_END');
                addToMap('SIGLA_END');
                addToMap('FK_COD_MUNICIPIO_END');
                addToMap('ID_ZONA_RESIDENCIAL');
                addToMap('ID_TIPO_ATENDIMENTO');
                addToMap('ID_N_T_E_P');
                addToMap('ID_RESPONSAVEL_TRANSPORTE');
                addToMap('ID_TRANSP_VANS_KOMBI');
                addToMap('ID_TRANSP_MICRO_ONIBUS');
                addToMap('ID_TRANSP_ONIBUS');
                addToMap('ID_TRANSP_BICICLETA');
                addToMap('ID_TRANSP_TR_ANIMAL');
                addToMap('ID_TRANSP_OUTRO_VEICULO');
                addToMap('ID_TRANSP_EMBAR_ATE5');
                addToMap('ID_TRANSP_EMBAR_5A15');
                addToMap('ID_TRANSP_EMBAR_15A35');
                addToMap('ID_TRANSP_EMBAR_35');
                addToMap('ID_TRANSP_TREM_METRO');
                addToMap('ID_POSSUI_NEC_ESPECIAL');
                addToMap('ID_TIPO_NEC_ESP_CEGUEIRA');
                addToMap('ID_TIPO_NEC_ESP_BAIXA_VISAO');
                addToMap('ID_TIPO_NEC_ESP_SURDEZ');
                addToMap('ID_TIPO_NEC_ESP_DEF_AUDITIVA');
                addToMap('ID_TIPO_NEC_ESP_SURDO_CEGUEIRA');
                addToMap('ID_TIPO_NEC_ESP_DEF_FISICA');
                addToMap('ID_TIPO_NEC_ESP_DEF_MENTAL');
                addToMap('ID_TIPO_NEC_ESP_DEF_MULTIPLAS');
                addToMap('ID_TIPO_NEC_ESP_AUTISMO');
                addToMap('ID_TIPO_NEC_ESP_ASPERGER');
                addToMap('ID_TIPO_NEC_ESP_RETT');
                addToMap('ID_TIPO_NEC_ESP_TDI');
                addToMap('ID_TIPO_NEC_ESP_SUPERDOTACAO');
                addToMap('FK_COD_MOD_ENSINO');
                addToMap('FK_COD_ETAPA_ENSINO');
                addToMap('PK_COD_TURMA');
                addToMap('FK_COD_CURSO_PROF');
                addToMap('COD_UNIFICADA');
                addToMap('FK_COD_TIPO_TURMA');
                addToMap('PK_COD_ENTIDADE');
                addToMap('FK_COD_ESTADO_ESCOLA');
                addToMap('SIGLA_ESCOLA');
                addToMap('COD_MUNICIPIO_ESCOLA');
                addToMap('FK_CODIGO_DISTRITO');
                addToMap('ID_LOCALIZACAO_ESC');
                addToMap('ID_DEPENDENCIA_ADM_ESC');
                addToMap('DESC_CATA_ESCOLA_PRIV');
                addToMap('ID_CONVENIADA_PP_ESC');
                addToMap('ID_TIPO_CONVENIO_PODER_PUBLICO');
                addToMap('ID_MANT_ESCOLA_PRIVADA_EMP');
                addToMap('ID_MANT_ESCOLA_PRIVADA_ONG');
                addToMap('ID_MANT_ESCOLA_PRIVADA_SIND');
                addToMap('ID_MANT_ESCOLA_PRIVADA_SIST_S');
                addToMap('ID_MANT_ESCOLA_PRIVADA_S_FINS');
                addToMap('ID_DOCUMENTO_REGULAMENTACAO');
                addToMap('ID_LOCALIZACAO_DIFERENCIADA');
                addToMap('ID_EDUCACAO_INDIGENA');

            case 2013
                addToMap('ID');
                addToMap('ANO_CENSO');
                addToMap('PK_COD_MATRICULA');
                addToMap('FK_COD_ALUNO');
                addToMap('NU_DIA');
                addToMap('NU_MES');
                addToMap('NU_ANO');
                addToMap('NUM_IDADE_REFERENCIA');
                addToMap('NUM_IDADE');
                addToMap('NU_DUR_ESCOLARIZACAO');
                addToMap('NU_DUR_ATIV_COMP_MESMA_REDE');
                addToMap('NU_DUR_ATIV_COMP_OUTRAS_REDES');
                addToMap('TP_SEXO');
                addToMap('TP_COR_RACA');
                addToMap('TP_NACIONALIDADE');
                addToMap('FK_COD_PAIS_ORIGEM');
                addToMap('FK_COD_ESTADO_NASC');
                addToMap('SGL_UF_NASCIMENTO');
                addToMap('FK_COD_MUNICIPIO_DNASC');
                addToMap('FK_COD_ESTADO_END');
                addToMap('SIGLA_END');
                addToMap('FK_COD_MUNICIPIO_END');
                addToMap('ID_ZONA_RESIDENCIAL');
                addToMap('ID_TIPO_ATENDIMENTO');
                addToMap('ID_N_T_E_P');
                addToMap('ID_RESPONSAVEL_TRANSPORTE');
                addToMap('ID_TRANSP_VANS_KOMBI');
                addToMap('ID_TRANSP_MICRO_ONIBUS');
                addToMap('ID_TRANSP_ONIBUS');
                addToMap('ID_TRANSP_BICICLETA');
                addToMap('ID_TRANSP_TR_ANIMAL');
                addToMap('ID_TRANSP_OUTRO_VEICULO');
                addToMap('ID_TRANSP_EMBAR_ATE5');
                addToMap('ID_TRANSP_EMBAR_5A15');
                addToMap('ID_TRANSP_EMBAR_15A35');
                addToMap('ID_TRANSP_EMBAR_35');
                addToMap('ID_TRANSP_TREM_METRO');
                addToMap('ID_POSSUI_NEC_ESPECIAL');
                addToMap('ID_TIPO_NEC_ESP_CEGUEIRA');
                addToMap('ID_TIPO_NEC_ESP_BAIXA_VISAO');
                addToMap('ID_TIPO_NEC_ESP_SURDEZ');
                addToMap('ID_TIPO_NEC_ESP_DEF_AUDITIVA');
                addToMap('ID_TIPO_NEC_ESP_SURDO_CEGUEIRA');
                addToMap('ID_TIPO_NEC_ESP_DEF_FISICA');
                addToMap('ID_TIPO_NEC_ESP_DEF_MENTAL');
                addToMap('ID_TIPO_NEC_ESP_DEF_MULTIPLAS');
                addToMap('ID_TIPO_NEC_ESP_AUTISMO');
                addToMap('ID_TIPO_NEC_ESP_ASPERGER');
                addToMap('ID_TIPO_NEC_ESP_RETT');
                addToMap('ID_TIPO_NEC_ESP_TDI');
                addToMap('ID_TIPO_NEC_ESP_SUPERDOTACAO');
                addToMap('ID_TIPO_REC_ESP_LEDOR');
                addToMap('ID_TIPO_REC_ESP_TRANSCRICAO');
                addToMap('ID_TIPO_REC_ESP_INTERPRETE');
                addToMap('ID_TIPO_REC_ESP_LIBRAS');
                addToMap('ID_TIPO_REC_ESP_LABIAL');
                addToMap('ID_TIPO_REC_ESP_BRAILLE');
                addToMap('ID_TIPO_REC_ESP_AMPLIADA_16');
                addToMap('ID_TIPO_REC_ESP_AMPLIADA_20');
                addToMap('ID_TIPO_REC_ESP_AMPLIADA_24');
                addToMap('ID_TIPO_REC_ESP_NENHUM');
                addToMap('ID_INGRESSO_FEDERAIS');
                addToMap('FK_COD_MOD_ENSINO');
                addToMap('FK_COD_ETAPA_ENSINO');
                addToMap('PK_COD_TURMA');
                addToMap('FK_COD_CURSO_PROF');
                addToMap('COD_UNIFICADA');
                addToMap('FK_COD_TIPO_TURMA');
                addToMap('PK_COD_ENTIDADE');
                addToMap('FK_COD_ESTADO_ESCOLA');
                addToMap('SIGLA_ESCOLA');
                addToMap('COD_MUNICIPIO_ESCOLA');
                addToMap('FK_CODIGO_DISTRITO');
                addToMap('ID_LOCALIZACAO_ESC');
                addToMap('ID_DEPENDENCIA_ADM_ESC');
                addToMap('DESC_CATA_ESCOLA_PRIV');
                addToMap('ID_CONVENIADA_PP_ESC');
                addToMap('ID_TIPO_CONVENIO_PODER_PUBLICO');
                addToMap('ID_MANT_ESCOLA_PRIVADA_EMP');
                addToMap('ID_MANT_ESCOLA_PRIVADA_ONG');
                addToMap('ID_MANT_ESCOLA_PRIVADA_SIND');
                addToMap('ID_MANT_ESCOLA_PRIVADA_SIST_S');
                addToMap('ID_MANT_ESCOLA_PRIVADA_S_FINS');
                addToMap('ID_DOCUMENTO_REGULAMENTACAO');
                addToMap('ID_LOCALIZACAO_DIFERENCIADA');
                addToMap('ID_EDUCACAO_INDIGENA');    
            case 2014           
                addToMap('ID')
                addToMap('ANO_CENSO')
                addToMap('PK_COD_MATRICULA')
                addToMap('FK_COD_ALUNO')
                addToMap('NU_DIA')
                addToMap('NU_MES')
                addToMap('NU_ANO')
                addToMap('NUM_IDADE_REFERENCIA')
                addToMap('NUM_IDADE')
                addToMap('NU_DUR_ESCOLARIZACAO')
                addToMap('NU_DUR_ATIV_COMP_MESMA_REDE')
                addToMap('NU_DUR_ATIV_COMP_OUTRAS_REDES')
                addToMap('NUM_DUR_AEE_MESMA_REDE')
                addToMap('NUM_DUR_AEE_OUTRAS_REDES')
                addToMap('TP_SEXO')
                addToMap('TP_COR_RACA')
                addToMap('TP_NACIONALIDADE')
                addToMap('FK_COD_PAIS_ORIGEM')
                addToMap('FK_COD_ESTADO_NASC')
                addToMap('FK_COD_MUNICIPIO_DNASC')
                addToMap('FK_COD_ESTADO_END')
                addToMap('FK_COD_MUNICIPIO_END')
                addToMap('ID_ZONA_RESIDENCIAL')
                addToMap('ID_TIPO_ATENDIMENTO')
                addToMap('ID_N_T_E_P')
                addToMap('ID_RESPONSAVEL_TRANSPORTE')
                addToMap('ID_TRANSP_VANS_KOMBI')
                addToMap('ID_TRANSP_MICRO_ONIBUS')
                addToMap('ID_TRANSP_ONIBUS')
                addToMap('ID_TRANSP_BICICLETA')
                addToMap('ID_TRANSP_TR_ANIMAL')
                addToMap('ID_TRANSP_OUTRO_VEICULO')
                addToMap('ID_TRANSP_EMBAR_ATE5')
                addToMap('ID_TRANSP_EMBAR_5A15')
                addToMap('ID_TRANSP_EMBAR_15A35')
                addToMap('ID_TRANSP_EMBAR_35')
                addToMap('ID_TRANSP_TREM_METRO')
                addToMap('ID_POSSUI_NEC_ESPECIAL')
                addToMap('ID_TIPO_NEC_ESP_CEGUEIRA')
                addToMap('ID_TIPO_NEC_ESP_BAIXA_VISAO')
                addToMap('ID_TIPO_NEC_ESP_SURDEZ')
                addToMap('ID_TIPO_NEC_ESP_DEF_AUDITIVA')
                addToMap('ID_TIPO_NEC_ESP_SURDO_CEGUEIRA')
                addToMap('ID_TIPO_NEC_ESP_DEF_FISICA')
                addToMap('ID_TIPO_NEC_ESP_DEF_MENTAL')
                addToMap('ID_TIPO_NEC_ESP_DEF_MULTIPLAS')
                addToMap('ID_TIPO_NEC_ESP_AUTISMO')
                addToMap('ID_TIPO_NEC_ESP_ASPERGER')
                addToMap('ID_TIPO_NEC_ESP_RETT')
                addToMap('ID_TIPO_NEC_ESP_TDI')
                addToMap('ID_TIPO_NEC_ESP_SUPERDOTACAO')
                addToMap('ID_TIPO_REC_ESP_LEDOR')
                addToMap('ID_TIPO_REC_ESP_TRANSCRICAO')
                addToMap('ID_TIPO_REC_ESP_INTERPRETE')
                addToMap('ID_TIPO_REC_ESP_LIBRAS')
                addToMap('ID_TIPO_REC_ESP_LABIAL')
                addToMap('ID_TIPO_REC_ESP_BRAILLE')
                addToMap('ID_TIPO_REC_ESP_AMPLIADA_16')
                addToMap('ID_TIPO_REC_ESP_AMPLIADA_20')
                addToMap('ID_TIPO_REC_ESP_AMPLIADA_24')
                addToMap('ID_TIPO_REC_ESP_NENHUM')
                addToMap('ID_INGRESSO_FEDERAIS')
                addToMap('FK_COD_MOD_ENSINO')
                addToMap('FK_COD_ETAPA_ENSINO')
                addToMap('ID_ETAPA_AGREGADA_MAT')
                addToMap('PK_COD_TURMA')
                addToMap('FK_COD_CURSO_PROF')
                addToMap('COD_UNIFICADA')
                addToMap('FK_COD_TIPO_TURMA')
                addToMap('PK_COD_ENTIDADE')
                addToMap('FK_COD_ESTADO_ESCOLA')
                addToMap('COD_MUNICIPIO_ESCOLA')
                addToMap('FK_CODIGO_DISTRITO')
                addToMap('ID_LOCALIZACAO_ESC')
                addToMap('ID_DEPENDENCIA_ADM_ESC')
                addToMap('DESC_CATA_ESCOLA_PRIV')
                addToMap('ID_CONVENIADA_PP_ESC')
                addToMap('ID_TIPO_CONVENIO_PODER_PUBLICO')
                addToMap('ID_MANT_ESCOLA_PRIVADA_EMP')
                addToMap('ID_MANT_ESCOLA_PRIVADA_ONG')
                addToMap('ID_MANT_ESCOLA_PRIVADA_SIND')
                addToMap('ID_MANT_ESCOLA_PRIVADA_SIST_S')
                addToMap('ID_MANT_ESCOLA_PRIVADA_S_FINS')
                addToMap('ID_DOCUMENTO_REGULAMENTACAO')
                addToMap('ID_LOCALIZACAO_DIFERENCIADA')
                addToMap('ID_EDUCACAO_INDIGENA')
            case 2015           
                addToMap('ID')
                addToMap('NU_ANO_CENSO')
                addToMap('ID_MATRICULA')
                addToMap('CO_PESSOA_FISICA')
                addToMap('NU_DIA')
                addToMap('NU_MES')
                addToMap('NU_ANO')
                addToMap('NU_IDADE_REFERENCIA')
                addToMap('NU_IDADE')
                addToMap('NU_DURACAO_TURMA')
                addToMap('NU_DUR_ATIV_COMP_MESMA_REDE')
                addToMap('NU_DUR_ATIV_COMP_OUTRAS_REDES')
                addToMap('NU_DUR_AEE_MESMA_REDE')
                addToMap('NU_DUR_AEE_OUTRAS_REDES')
                addToMap('NU_DIAS_ATIVIDADE')
                addToMap('TP_SEXO')
                addToMap('TP_COR_RACA')
                addToMap('TP_NACIONALIDADE')
                addToMap('CO_PAIS_ORIGEM')
                addToMap('CO_UF_NASC')
                addToMap('CO_MUNICIPIO_NASC')
                addToMap('CO_UF_END')
                addToMap('CO_MUNICIPIO_END')
                addToMap('TP_ZONA_RESIDENCIAL')
                addToMap('TP_OUTRO_LOCAL_AULA')
                addToMap('IN_TRANSPORTE_PUBLICO')
                addToMap('TP_RESPONSAVEL_TRANSPORTE')
                addToMap('IN_TRANSP_VANS_KOMBI')
                addToMap('IN_TRANSP_MICRO_ONIBUS')
                addToMap('IN_TRANSP_ONIBUS')
                addToMap('IN_TRANSP_BICICLETA')
                addToMap('IN_TRANSP_TR_ANIMAL')
                addToMap('IN_TRANSP_OUTRO_VEICULO')
                addToMap('IN_TRANSP_EMBAR_ATE5')
                addToMap('IN_TRANSP_EMBAR_5A15')
                addToMap('IN_TRANSP_EMBAR_15A35')
                addToMap('IN_TRANSP_EMBAR_35')
                addToMap('IN_TRANSP_TREM_METRO')
                addToMap('IN_NECESSIDADE_ESPECIAL')
                addToMap('IN_CEGUEIRA')
                addToMap('IN_BAIXA_VISAO')
                addToMap('IN_SURDEZ')
                addToMap('IN_DEF_AUDITIVA')
                addToMap('IN_SURDOCEGUEIRA')
                addToMap('IN_DEF_FISICA')
                addToMap('IN_DEF_INTELECTUAL')
                addToMap('IN_DEF_MULTIPLA')
                addToMap('IN_AUTISMO')
                addToMap('IN_SINDROME_ASPERGER')
                addToMap('IN_SINDROME_RETT')
                addToMap('IN_TRANSTORNO_DI')
                addToMap('IN_SUPERDOTACAO')
                addToMap('IN_RECURSO_LEDOR')
                addToMap('IN_RECURSO_TRANSCRICAO')
                addToMap('IN_RECURSO_INTERPRETE')
                addToMap('IN_RECURSO_LIBRAS')
                addToMap('IN_RECURSO_LABIAL')
                addToMap('IN_RECURSO_BRAILLE')
                addToMap('IN_RECURSO_AMPLIADA_16')
                addToMap('IN_RECURSO_AMPLIADA_20')
                addToMap('IN_RECURSO_AMPLIADA_24')
                addToMap('IN_RECURSO_NENHUM')
                addToMap('TP_INGRESSO_FEDERAIS')
                addToMap('TP_MEDIACAO_DIDATICO_PEDAGO')
                addToMap('IN_ESPECIAL_EXCLUSIVA')
                addToMap('IN_REGULAR')
                addToMap('IN_EJA')
                addToMap('IN_PROFISSIONALIZANTE')
                addToMap('TP_ETAPA_ENSINO')
                addToMap('TP_ETAPA_AGREGADA')
                addToMap('ID_TURMA')
                addToMap('CO_CURSO_EDUC_PROFISSIONAL')
                addToMap('TP_UNIFICADA')
                addToMap('TP_TIPO_TURMA')
                addToMap('CO_ENTIDADE')
                addToMap('CO_REGIAO')
                addToMap('CO_MESORREGIAO')
                addToMap('CO_MICRORREGIAO')
                addToMap('CO_UF')
                addToMap('CO_MUNICIPIO')
                addToMap('CO_DISTRITO')
                addToMap('TP_DEPENDENCIA')
                addToMap('TP_LOCALIZACAO')
                addToMap('TP_CATEGORIA_ESCOLA_PRIVADA')
                addToMap('IN_CONVENIADA_PP')
                addToMap('TP_CONVENIO_PODER_PUBLICO')
                addToMap('IN_MANT_ESCOLA_PRIVADA_EMP')
                addToMap('IN_MANT_ESCOLA_PRIVADA_ONG')
                addToMap('IN_MANT_ESCOLA_PRIVADA_SIND')
                addToMap('IN_MANT_ESCOLA_PRIVADA_SIST_S')
                addToMap('IN_MANT_ESCOLA_PRIVADA_S_FINS')
                addToMap('TP_REGULAMENTACAO')
                addToMap('TP_LOCALIZACAO_DIFERENCIADA')
                addToMap('IN_EDUCACAO_INDIGENA')                
        end
        
        labelsMap = map;
end

function addToMap(key)
    global index map
    index = index + 1;
    map(key) = index;
end