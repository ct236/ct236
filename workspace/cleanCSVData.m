function data = cleanCSVData(year, csvpath)

    labels = getLabels(year);
    
    cleanedDatapath = [csvpath(1 : max(strfind(csvpath, '.')) - 1), '_clean.csv'];
    fprintf('Lendo %s e convertendo para %s...', csvpath, cleanedDatapath);
    
    if(exist(cleanedDatapath, 'file'))
        out = fopen(cleanedDatapath, 'w+');
        fprintf(out, '');
        fclose(out);
    end

    in = fopen(csvpath, 'r');
    out = fopen(cleanedDatapath, 'a+');

    line = fgetl(in);
        
    qtyOfAttributes = size(labels, 1);
    
    attributes = length(strfind(line, ','));
    if(attributes == qtyOfAttributes - 1)
        commaSeparated = true;
    else
        attributes = length(strfind(line, ';'));
        if(attributes == qtyOfAttributes - 1)
            commaSeparated = false;
        else
            error('Formato não reconhecido');
        end
    end
    
    line = fgetl(in);
    while ischar(line)
        line = strrep(line, '"', '');
        line = strrep(line, '\"', '');
        
        if(~commaSeparated)
            line = strrep(line, ',', '.');
            line = strrep(line, ';', ',');
        end
        
        line = strrep(line, ',,', ',##_##,');        
        line = regexprep(line, ',\s*,', ',##_##,');
        line = strrep(line, ',NA,', ',##_##,');
        line = strrep(line, ',,', ',');  
        
        line = chars2numbers(line);
        line = strrep(line, '##_##', 'nan');        
        
        fprintf(out, '%s\n', line);
        
        line = fgetl(in);
    end
    fclose(in);
    fclose(out);
    
    data = csvread(cleanedDatapath);

end