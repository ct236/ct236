function main(saveToPajekFormat, executeWithProfile)
    clearvars -except saveToPajekFormat executeWithProfile
    clear global 

    if(nargin == 0)
        saveToPajekFormat = false;
    end        
    if(nargin ~= 2)
        executeWithProfile = false;
    end
    
        execute(2012, 'Corumba/dados/2012.csv', 0, saveToPajekFormat, executeWithProfile);    
        execute(2013, 'Corumba/dados/2013.csv', 0, saveToPajekFormat, executeWithProfile);  
        execute(2014, 'Corumba/dados/2014.csv', 0, saveToPajekFormat, executeWithProfile);  
        execute(2015, 'Corumba/dados/2015.csv', 0, saveToPajekFormat, executeWithProfile);  
   
end

function execute(year, dataFilepath, maxDistances, saveToPajekFormat, executeWithProfile)
       
    if(executeWithProfile)
        profile off
        profile on
    end

    globalTic = tic;
    doClustering(year, saveToPajekFormat, dataFilepath, getAttsAndWeights(year), maxDistances);        
    fprintf('Tempo total de execução: %f s', toc(globalTic));

    if(executeWithProfile)
        profile viewer
    end

end