function [G, D] = outcome2Graph(outcome)
    G = graph(toSparseMatrix(outcome.edges, outcome.n));
    D = degree(G);
end