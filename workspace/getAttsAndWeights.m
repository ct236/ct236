function attsAndWeights = getAttsAndWeights(year)
    labels = getLabels(year);
    
    if(max(year == [2012, 2013, 2014]))
            attsAndWeights = [
                        labels('NU_DUR_ATIV_COMP_MESMA_REDE'), 1;
                        labels('NU_DUR_ATIV_COMP_OUTRAS_REDES'), 1;
                        labels('FK_COD_MOD_ENSINO'), 1;            
                        labels('FK_COD_ETAPA_ENSINO'), 2;            
                        labels('PK_COD_TURMA'), 1;       
                        labels('FK_COD_TIPO_TURMA'), 2;
                        labels('ID_DEPENDENCIA_ADM_ESC'), 3;
                        ];	
    else
        if year == 2015
            attsAndWeights = [
                        labels('NU_DUR_ATIV_COMP_MESMA_REDE'), 1;
                        labels('NU_DUR_ATIV_COMP_OUTRAS_REDES'), 1;                               
                        labels('IN_ESPECIAL_EXCLUSIVA'), 1;            
                        labels('IN_REGULAR'), 1;
                        labels('IN_EJA'), 1;
                        labels('TP_ETAPA_ENSINO'), 2;            
                        labels('ID_TURMA'), 1;       
                        labels('TP_TIPO_TURMA'), 2;
                        labels('TP_DEPENDENCIA'), 3;
                        ];  
        end
    end
end