function doClustering(year, saveToPajekFormat, dataPath, attsAndWeights, maxDistances)
    global data maxAttributeValues filenamePreffix
    
    fprintf('\n>> Iniciando em %s...\n', datetime);
    
    cachePath = 'cache';
    
    if isnumeric(year), year = num2str(year); end
    
    outcomePath = 'resultados';
    if(~exist(outcomePath, 'dir'))
        mkdir(outcomePath);
    end
    
    if(~exist(cachePath, 'dir'))
        mkdir(cachePath);
    end
    
    cleanedDataPath = [dataPath(1 : max(strfind(dataPath, '.')) - 1), '_clean.csv'];
    if(~exist(cleanedDataPath, 'file'))
        data = cleanCSVData(year, dataPath);
    else
        data = csvread(cleanedDataPath);
    end   
    
    maxAttributeValues = max(data);
    maxAttributeValues(maxAttributeValues == 0) = 1; 
        
    filenamePreffix = sprintf('%s/y_%s_n%d_d', outcomePath, year, size(data,1));
          
    performAndSave(year, saveToPajekFormat, maxDistances, attsAndWeights);
    fprintf('\n>> Finalizado em %s.\n', datetime);
end

function allComponents = performAndSave(year, saveToPajekFormat, maxDistances, attsAndWeights)
   global filenamePreffix
      
   attsAndWeights(:,2) = attsAndWeights(:,2) ./ sum(attsAndWeights(:, 2));     
    
    allComponents = cell(length(maxDistances),1);
    for i = 1 : length(maxDistances)
        clear outcome;
        partialTic = tic;
        fprintf('\n>> Identificando rede %d/%d para norma 1 máxima dos atributos selecinados de %s...', i, length(maxDistances), num2str(maxDistances(i)));
        outcome = performClustering(year, maxDistances(i), attsAndWeights(:,1), attsAndWeights(:,2));
        
        filename = sprintf('%s%s', filenamePreffix, num2str(outcome.maxDistance));
        save([filename '.mat'], 'outcome');
        
        if(saveToPajekFormat)
            toPajek([filename '.net'], outcome);
        end
        
        outcome.components = [];
        outcome.labels = [];    
        outcome.attributesIndexes = [];
        outcome.attributesWeights = [];
        outcome.edgesDistances = [];
        csvFilename = [filename '.csv'];
        dlmwrite(csvFilename, [outcome.edges; outcome.edges(:,2), outcome.edges(:,1)], 'delimiter', ',', 'precision', 9);
                
        fprintf('\n\tRede identificada com %d vértices, %d arestas, comp. gigante com %d vértices e salva em %f s\n', outcome.n, size(outcome.edges,1), outcome.giantComponentSize, toc(partialTic));
        
        clear outcome
        load gong;
        sound(y);
    end
end

function outcome = performClustering(year, maxDistance, atts, weights)
    global data maxAttributeValues
         
    n = size(data, 1);
    outcome.edges = nan(n, 2);
    edgesSize = 0;        
    outcome.edgesDistances = nan(n, 1);
    
    n = size(outcome.edges, 1);    
    imax = n - 1;
    
    statusStep = round(n / 5);    
    wbar = waitbar(0, sprintf('Gerando redes com maxDistance=%f', maxDistance));
    overallEdgesSize = nchoosek(n, 2);
    
    for i = 1 : imax    
        if(mod(i, statusStep) == 0)
            fprintf('\n>> [Arestas atuais] / [Arestas totais] = %f%', edgesSize / overallEdgesSize);
            waitbar(i/imax);
        end
              
        distances = sum(abs(data(:,atts) - data(i, atts)) .* weights' ./ maxAttributeValues(atts) , 2);
        neighboors = find(distances <= maxDistance);        
        neighboors(neighboors <= i) = [];        
        neighboorsSize = length(neighboors);
        
        if(neighboorsSize == 0)
            continue;
        end
        
        edgesSize = edgesSize + 1;
        newEdgesSize = edgesSize + neighboorsSize - 1;

        outcome.edges(edgesSize : newEdgesSize, :) = [i * ones(neighboorsSize, 1), neighboors];        
        outcome.edgesDistances(edgesSize : newEdgesSize) = distances(neighboors);
        edgesSize = newEdgesSize;       
    end
        
    waitbar(1);
    clear distances neighboors
    clear global data    
            
    outcome.edges(isnan(outcome.edges(:,1)), :) = [];   
    outcome.n = n;
    outcome.maxDistance = maxDistance;        
    outcome.labels = getLabels(year);
    outcome.attributesIndexes = atts;
    outcome.attributesWeights = weights;
    
    cache = 'cache/~temp.mat';
    save(cache, 'outcome');   
    try         
        sparseMatrix = sparse(outcome.edges(:,1), outcome.edges(:,2), 1, n, n);
        clear outcome
        sparseMatrix = sparseMatrix + sparseMatrix';   
        G = graph(sparseMatrix);
        clear sparseMatrix;
        components = conncomp(G);
        degrees = degree(G);
        clear G;
        giantComponentSize = length(components(components == mode(components)));
    catch
        fprintf('\n>> Não foi possível gerar os componentes a partir de outcome.edges por falta de memoria!');
    end
    
    if(~exist('outcome', 'var'))
        load(cache);
    end
    
    fprintf('\n>>Max Degree: %d', max(degrees));
    
    if(exist('components', 'var') && exist('giantComponentSize', 'var') )
        outcome.components = components;
        outcome.giantComponentSize = giantComponentSize;
        outcome.degrees = degrees;
    else
        outcome.components = [];
        outcome.giantComponentSize = NaN;
        outcome.degrees = [];
    end
    
    clear components giantComponentSize
    delete(cache);
    close(wbar);    
end