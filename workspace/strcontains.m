function contains = strcontains(string, pattern)
    contains = ~isempty(strfind(string, pattern));
end