function sparseMatrix = toSparseMatrix(edges, n)
    sparseMatrix = sparse(edges(:,1), edges(:,2), repmat(length(edges(:,1)), 1), n, n);
    sparseMatrix = sparseMatrix + sparseMatrix';
end